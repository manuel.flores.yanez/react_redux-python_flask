import React from 'react';
import './App.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Items from './components/items';

function App() {
  return (
    <div className="App">
      <React.Fragment>
        <CssBaseline />
        <AppBar>       
          <Toolbar>
            <Typography variant="h6">Item Inventory</Typography>
          </Toolbar>
        </AppBar>
        <Toolbar />
        <Container>
          <Box my={2}>
            <Items />
          </Box>
        </Container>
      </React.Fragment>
    </div>
  );
}

export default App;
