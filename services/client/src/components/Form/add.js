import React from 'react';
import { useContext } from 'react';
import { AddContext } from '../items';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import InputAdornment from '@material-ui/core/InputAdornment';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 200,
        },
    },
    dialogForm: {
        width: 544,
        margin: 'auto'
    }
}));

export default function AlertDialogSlide() {
    const addItem = useContext(AddContext);
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState('');
    const [brand, setBrand] = React.useState('');
    const [manufacturer, setManufacturer] = React.useState('');
    const [mymodel, setMymodel] = React.useState('');
    const [price, setPrice] = React.useState('')
    const classes = useStyles();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setName('');
        setBrand('');
        setManufacturer('');
        setMymodel('');
        setPrice('');
    };

    const submitHandler = e => {
        e.preventDefault();
        let myPrice = price;
        if (myPrice.substring(myPrice.length, myPrice.length + 1) === '.') {
            myPrice = `${myPrice}0`;
        }
        if (myPrice.substring(0, 1) === '.') {
            myPrice = `0${myPrice}`;
        }

        const newItem = {
            name: name,
            brand: brand,
            manufacturer: manufacturer,
            mymodel: mymodel,
            price: myPrice
        }
        addItem(newItem);
        handleClose();
    }

    const checkPrice = myPrice => {
        const myNewPrice = myPrice;
        let counter = 0;
        //Checks if the number is constrained to certain criteria
        for (let index = 0; index < myNewPrice.length; index++) {
            if (myNewPrice[index] === '.') {
                counter = counter + 1;
            }
            if (counter === 2) {
                myPrice = myNewPrice.slice(0, index) + myNewPrice.slice(index + 1)
                index = myNewPrice.length;
            }
        }
        myPrice = myPrice.replace(/[^0-9$.]/g, '');
        setPrice(myPrice)
    }

    return (
        <div>
            <Fab size="medium" color="secondary" aria-label="add" onClick={handleClickOpen}>
                <AddIcon />
            </Fab>
            <Dialog
                className={classes.dialogForm}
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Add a new Item"}</DialogTitle>
                <DialogContent >
                    <form onSubmit={submitHandler} className={classes.root} autoComplete="off" >
                        <TextField
                            required
                            id="filled-name"
                            label="Name"
                            variant="filled"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                        <TextField
                            required
                            id="filled-brand"
                            label="Brand"
                            variant="filled"
                            value={brand}
                            onChange={e => setBrand(e.target.value)}
                        />
                        <TextField
                            required
                            id="filled-manufacturer"
                            label="Manufacturer"
                            variant="filled"
                            value={manufacturer}
                            onChange={e => setManufacturer(e.target.value)}
                        />
                        <TextField
                            required
                            id="filled-model"
                            label="Model"
                            variant="filled"
                            value={mymodel}
                            onChange={e => setMymodel(e.target.value)}
                        />
                        <TextField
                            id="filled-price"
                            label="Price"
                            variant="filled"
                            value={price}
                            onChange={e => checkPrice(e.target.value)}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            }}
                        />
                        <DialogActions>
                            <Button

                                variant="contained"
                                color="default"
                                onClick={handleClose}
                                size="small"
                            >
                                Cancel
      </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                size="small"
                                type="submit"
                                startIcon={<SaveIcon />}
                            >
                                Save
      </Button>
                        </DialogActions>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}