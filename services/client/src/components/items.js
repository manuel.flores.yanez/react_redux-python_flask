import React from 'react';
import axios from 'axios';
import { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import AddButton from './Form/add';
import DeleteButton from './Form/delete';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../state/index';

const columns = [
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'brand', label: 'Brand', minWidth: 100 },
    {
        id: 'manufacturer',
        label: 'Manufacturer',
        minWidth: 170,
        align: 'right',
        format: value => value.toLocaleString(),
    },
    {
        id: 'mymodel',
        label: 'Model',
        minWidth: 170,
        align: 'right',
        format: value => value.toLocaleString(),
    },
    {
        id: 'price',
        label: 'Price',
        minWidth: 170,
        align: 'right',
        format: value => value.toFixed(2),
    },
    {
        id: 'add',
        label: 'Add',
        minWidth: 170,
        align: 'right',
        format: value => value.toLocaleString(),
    },
];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 900,
    },
});

export const AddContext = React.createContext();
export const DeleteContext = React.createContext();
export default function StickyHeadTable() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    //This is the state inside of the store
    const state = useSelector((state) => state);
    const dispatch = useDispatch();

    // These are my Action Creators
    const ActionCreators = bindActionCreators(actionCreators, dispatch);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    useEffect(() => {
        axios.get('http://localhost:80/items')
            .then(response => {
                let newRows = response.data;
                newRows.sort((a, b) => a.name.localeCompare(b.name));
                ActionCreators.setRows([...newRows])
            })
            .catch(error => {
                console.log(error)
            })
    }, [])


    const addItem = newItem => {
        axios.post('http://localhost:80/item', newItem)
            .then(response => {
                let rowReturned = response.data;
                let newRows = [...state.rows]
                newRows.push(rowReturned)
                newRows.sort((a, b) => a.name.localeCompare(b.name));
                ActionCreators.setRows([...newRows])
            })
            .catch(error => {
                console.log(error)
            })
    }

    const deleteItem = () => {
        axios.delete(`http://localhost:80/item/${state.rowId}`)
            .then(response => {
                let newRows = [...state.rows]
                let foundIndex = newRows.findIndex((a) => {
                    return a.id === state.rowId
                })
                newRows.splice(foundIndex, 1);
                ActionCreators.setRows([...newRows])
            })
            .catch(error => {
                console.log(error)
            })
    };

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map(column => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label === 'Add' ?
                                        <AddContext.Provider value={addItem}>
                                            <AddButton />
                                        </AddContext.Provider>
                                        : column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {state.rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                    {columns.map(column => {
                                        return (
                                            <TableCell key={column.id} align={column.align} >
                                                {column.id === "price" ? `$` : null}
                                                {column.id === "add" ?
                                                    <div onClick={() => ActionCreators.setRowId(row.id)}>
                                                        <DeleteContext.Provider value={deleteItem}  >
                                                            <DeleteButton />
                                                        </DeleteContext.Provider>
                                                    </div>
                                                    :
                                                    row[column.id]}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={state.rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper >
    );
}