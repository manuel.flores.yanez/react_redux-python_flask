export const setRowId = (rowId) => {
    return dispatch => {
        dispatch({
            type: "setRowId",
            payload: rowId
        })
    }
}

export const setRows = (rows) => {
    return dispatch => {
        dispatch({
            type: "setRows",
            payload: rows
        })
    }
}