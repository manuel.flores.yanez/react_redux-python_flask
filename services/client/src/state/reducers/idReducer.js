const idReducer = (state = 0, action) => {
    switch (action.type) {
        case 'setRowId':
            return action.payload
        default:
            return state
    }
}

export default idReducer;