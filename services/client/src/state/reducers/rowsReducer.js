const rowsReducer = (state = [], action) => {
    switch (action.type) {
        case 'setRows':
            return action.payload
        default:
            return state
    }
}

export default rowsReducer;