import { combineReducers } from "redux";
import rowsReducer from "./rowsReducer";
import idReducer from "./idReducer";

const reducers = combineReducers({
    rowId: idReducer,
    rows: rowsReducer
});

export default reducers