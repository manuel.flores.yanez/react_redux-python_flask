from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

class Items(db.Model):
    __tablename__='items'
    
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(100))
    brand = db.Column(db.String(100))
    manufacturer = db.Column(db.String(100))
    model = db.Column(db.String(100))
    price = db.Column(db.Integer)
    
    def __init__(self, name, brand, manufacturer, model, price, id):
        self.name = name
        self.brand = brand
        self.manufacturer = manufacturer
        self.model = model
        self.price = price
        self.id = id