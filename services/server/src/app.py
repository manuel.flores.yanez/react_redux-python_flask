from flask import Flask, request, jsonify
import uuid
from flask_marshmallow import Marshmallow
from flask_cors import CORS, cross_origin
from conf.config import Config
from models.models import db, Items

app = Flask(__name__)
cors = CORS(app)
app.config.from_object(Config)
db.init_app(app)
mashmallow = Marshmallow(app)

class ItemsSchema(mashmallow.Schema):
    class Meta:
        fields = ('name', 'brand', 'manufacturer', 'model', 'price', 'id')

item_schema = ItemsSchema()
items_schema = ItemsSchema(many=True)

@app.route('/items', methods=['GET'])
@cross_origin()
def obtain_items():
    all_items = Items.query.all()
    return items_schema.jsonify(all_items)


@app.route('/item/<id>', methods=['PUT'])
@cross_origin()
def update_one_item(id):
    item = Items.query.get(id)
    name = request.json['name']
    brand = request.json['brand']
    manufacturer = request.json['manufacturer']
    model = request.json['mymodel']
    price = request.json['price']
    if name is not None:
        item.name = name
    if brand is not None:
        item.brand = brand
    if manufacturer is not None:
        item.manufacturer = manufacturer
    if model is not None:
        item.model = model
    if price is not None:
        item.price = price
    db.session.commit()
    return item_schema.jsonify(item)

@app.route('/item/<rowId>', methods=['DELETE'])
@cross_origin()
def delete_one_item(rowId):
    item = Items.query.filter_by(id=rowId).delete()
    db.session.commit()
    return item_schema.jsonify(item)


@app.route('/item', methods=['POST'])
@cross_origin()
def create_task():
    name = request.json['name']
    brand = request.json['brand']
    manufacturer = request.json['manufacturer']
    model = request.json['mymodel']
    price = request.json['price']
    id = str(uuid.uuid4())
    new_item = Items(name, brand, manufacturer, model, price,  id)
    db.session.add(new_item)
    db.session.commit()
    jsonReturn = jsonify({"name": new_item.name, "brand": new_item.brand, "manufacturer": new_item.manufacturer,
                          "model": new_item.model, "price": new_item.price, "id": id})
    return jsonReturn


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
