# REST API Node JS - React - Mongo DB.
# MIT License


### Setup
Run the server through Docker-Compose:
```bash
docker-compose up
```
Then it will automatically made available the server in `http://localhost:1234/`

## Item Inventory
Visit `http://localhost:1234/` to view the Item Inventory

### Usage
By opening the startup page, all items are automatically listed.

![](./docs/MainMenu.png)

### Create an Item
In order to create an Item, we must press the button on the upper right corner of the page:

![ListItems](docs/CreateButton.png)

Then we have to add all the data required to be allowed to save this Item.

![](docs/CreateItem.png)
Use cases: If we add a number beginning with a dot `.`, the application will assume there is a zero before the dot.
Example:  
If we enter `.95`, the program will save the price as `0.95`.
No more than one dot is allowed, in order to place our dot in another place of the number, we must delete the other dot first.

### Delete an Item
The following button helps us delete an item:

![DeleteButton](docs/DeleteButton.png)


By pressing the delete button, we will be asked if we are sure to perform this action, by pressing yes the Item will be deleted from the database.
![DeleteItem](docs/DeleteItem.png)

The table is automatically updated after we create or delete an item.

### CI / CD 
This project is Dockerized in Docker compose, and I also added a gitlab-ci.yml file, this allows us to run the application on Gitlab Runner.